#!/bin/bash
root="rosa-cim-providers-0.1"
src="src"
src_tar_xz="./$root.tar.xz"

rm -rf ./$src_tar_xz

mkdir ./$root
mkdir ./$root/$src
mkdir ./$root/cmake
mkdir ./$root/mof

cp -f README COPYING CMakeLists.txt rosa-cim-register ./$root
cp -R ./$src/* ./$root/$src
cp -R ./cmake/* ./$root/cmake
cp ./mof/* ./$root/mof


tar -cvJf $src_tar_xz "./$root"

rpmbuild --define="_sourcedir `pwd`"  rosa-cim-providers.spec --ba
rm -rf ./$root