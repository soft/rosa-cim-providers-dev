set(PROVIDERS_MODULE "RLMILocaleModule")
set(LIBRARY_NAME cmpiRLMI_Locale)
set(MOFS 60_RLMI_Locale.mof)
set(SRCS "")

source_module_process(${PROVIDERS_MODULE} ${LIBRARY_NAME} "${MOFS}" "${SRCS}" "" "")
