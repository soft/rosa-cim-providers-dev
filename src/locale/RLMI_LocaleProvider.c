#include <konkret/konkret.h>
#include "RLMI_Locale.h"
#include "rosacim-helpers.h"

#include <ctype.h>

static const CMPIBroker* _cb = NULL;

static void RLMI_LocaleInitialize()
{
}

static CMPIStatus RLMI_LocaleCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_LocaleEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus RLMI_LocaleEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    char line[1025];
    char *tstr;
    char *sstr;
    char *name;
    char *val;
    FILE *fp;
    RLMI_Locale obj;
    int locstate = 0;

    fp = popen("locale -a -v", "r");
    if (!fp) CMReturn(CMPI_RC_ERR_FAILED);

    while (fgets(line, sizeof(line)-1, fp) != NULL) {
        if ((tstr = (char *)substr_with_prefix(line, "locale:")) != NULL) {
            if (locstate) {
                KReturnInstance(cr, obj);
            }
            tstr = strstrip(tstr);
            if (*tstr) {
                for (sstr = tstr; *sstr; sstr++)
                    if (isspace(*sstr)) break;
                *sstr = 0;
                sstr++;
                sstr = strstrip(sstr);
                if (substr_with_prefix(sstr, "directory:")) {
                    RLMI_Locale_Init(&obj, _cb, KNameSpace(cop));
                    RLMI_Locale_Set_SettingID(&obj, tstr);
                    locstate = 1;
                }
            }
        } else if (locstate) {
            val = line;
            name = strsep(&val, "|");
            if (name != NULL && val != NULL) {
                 name = strstrip(name);
                 val = strstrip(val);
                 if (strcasecmp(name, "title") == 0 && *val)
                     RLMI_Locale_Set_Caption(&obj, val);
                 else if (strcasecmp(name, "language") == 0 && *val)
                     RLMI_Locale_Set_Language(&obj, val);
                 else if (strcasecmp(name, "territory") == 0 && *val)
                     RLMI_Locale_Set_Territory(&obj, val);
                 else if (strcasecmp(name, "revision") == 0 && *val)
                     RLMI_Locale_Set_Revision(&obj, val);
                 else if (strcasecmp(name, "codeset") == 0 && *val)
                     RLMI_Locale_Set_CodeSet(&obj, val);
            }
        }
    }
    if (locstate) {
        KReturnInstance(cr, obj);
    }

    pclose(fp);
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_LocaleGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_LocaleCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_LocaleModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_LocaleDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_LocaleExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    RLMI_Locale,
    RLMI_Locale,
    _cb,
    RLMI_LocaleInitialize())

static CMPIStatus RLMI_LocaleMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_LocaleInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return RLMI_Locale_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

CMMethodMIStub(
    RLMI_Locale,
    RLMI_Locale,
    _cb,
    RLMI_LocaleInitialize())

KUint32 RLMI_Locale_VerifyOKToApplyToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_LocaleRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_Locale_ApplyToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_LocaleRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_Locale_VerifyOKToApplyToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_LocaleRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_Locale_ApplyToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_LocaleRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KBoolean* ContinueOnError,
    const KDateTime* MustBeCompletedBy,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_Locale_VerifyOKToApplyIncrementalChangeToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_LocaleRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_Locale_ApplyIncrementalChangeToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_LocaleRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_Locale_VerifyOKToApplyIncrementalChangeToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_LocaleRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_Locale_ApplyIncrementalChangeToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_LocaleRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KBoolean* ContinueOnError,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_Locale",
    "RLMI_Locale",
    "instance method")
