#include <konkret/konkret.h>
#include "RLMI_VConsoleKeymap.h"
#include "rosacim-helpers.h"

#include <dirent.h>
#include <stdio.h>
#include <glib.h>
#include <sys/stat.h>

#define KBDMAPS_PATH "/usr/lib/kbd/keymaps"

static const CMPIBroker* _cb = NULL;

static void RLMI_VConsoleKeymapInitialize()
{
}

static CMPIStatus RLMI_VConsoleKeymapCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_VConsoleKeymapEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static int scan_dir_for_keymaps(const char *path, const char *dpath, GData **gdl) {
    struct dirent des;
    struct dirent *de;
    struct stat stat_buf;
    char *newpath;
    char *newdpath;
    char *kme;
    char *kmcat;
    gpointer kmptr;

    DIR *d = opendir(path);
    if (!d) return -1;
    int ret = 0;
    int err = 0;
    do {
        err = readdir_r(d, &des, &de);
        if (err) {
            ret = -1;
            break;
        }
        if (de == NULL) break;
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;
        ret = asprintf(&newpath, "%s/%s", path, de->d_name);
        if (ret < 0) break;
        ret = stat(newpath, &stat_buf);
        if (ret == 0) {
            if (S_ISDIR(stat_buf.st_mode)) {
                // Skip "include" directory as it doesn't contain full keymaps
                if (strcmp(de->d_name, "include") != 0) {
                    if (*dpath) ret = asprintf(&newdpath, "%s/%s", dpath, de->d_name);
                    else newdpath = de->d_name;
                    if (ret >= 0) {
                        ret = scan_dir_for_keymaps(newpath, newdpath, gdl);
                        if (newdpath != de->d_name)
                            free(newdpath);
                    }
                }
            } else {
                // The same extensions that localectl checks
                kme = endswith(de->d_name, ".map.gz");
                if (!kme) kme = endswith(de->d_name, ".map");
                if (kme) {
                    *kme = 0;
                    if ((kmptr = g_datalist_get_data(gdl, de->d_name)) != NULL) {
                        if ((ret = asprintf(&kmcat, "%s//%s", (char *)kmptr, dpath)) < 0)
                            kmcat = NULL;
                        free(kmptr);
                    } else
                        kmcat = strdup(dpath);
                    if (kmcat)
                        g_datalist_set_data(gdl, de->d_name, kmcat);
                    else ret = - 1;
                }
            }
        }
        free(newpath);
    } while (ret >= 0);
    closedir(d);
    return ret;
}

static void list_keymaps(GQuark key_id, gpointer data, gpointer user_data) {
    const gchar *strKey;
    strKey = g_quark_to_string(key_id);
    GSList **gslist = (GSList **)user_data;
    *gslist = g_slist_append(*gslist, (char *)strKey);
}

static CMPIStatus RLMI_VConsoleKeymapEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    GData *gdl;
    GSList *gslist = NULL, *clist;
    char *kmptr;
    int ret;
    RLMI_VConsoleKeymap obj;

    g_datalist_init(&gdl);
    gslist = NULL;

    ret = scan_dir_for_keymaps(KBDMAPS_PATH, "", &gdl);
    if (ret >= 0) {
        g_datalist_foreach(&gdl, list_keymaps, &gslist);
        gslist = g_slist_sort(gslist, (GCompareFunc)g_ascii_strcasecmp);
        clist = gslist;

        while (clist) {
            const char *kmname = (const char *)clist->data;
            clist = g_slist_next(clist);
            kmptr = g_datalist_get_data(&gdl, kmname);
            if (kmptr) {
                int n = 0;
                char *cp = kmptr;
                if (*cp) for (++n; (cp = strstr(cp, "//")) != NULL; ++n, cp += 2);
                RLMI_VConsoleKeymap_Init(&obj, _cb, KNameSpace(cop));
                RLMI_VConsoleKeymap_Set_SettingID(&obj, kmname);
                RLMI_VConsoleKeymap_Init_Categories(&obj, n);
                if (n > 0) {
                    char *pcp;
                    for (n = 0, cp = kmptr, pcp = cp;
                         (cp = strstr(cp, "//")) != NULL;
                         ++n, cp += 2, pcp = cp) {
                        *cp = 0;
                        RLMI_VConsoleKeymap_Set_Categories(&obj, n, pcp);
                    }
                    RLMI_VConsoleKeymap_Set_Categories(&obj, n, pcp);
                }
                KReturnInstance(cr, obj);
                free(kmptr);
            }
        }
    }

    g_slist_free(gslist);
    g_datalist_clear(&gdl);

    if (ret < 0) CMReturn(CMPI_RC_ERR_FAILED);
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_VConsoleKeymapGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_VConsoleKeymapCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_VConsoleKeymapModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_VConsoleKeymapDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_VConsoleKeymapExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    RLMI_VConsoleKeymap,
    RLMI_VConsoleKeymap,
    _cb,
    RLMI_VConsoleKeymapInitialize())

static CMPIStatus RLMI_VConsoleKeymapMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_VConsoleKeymapInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return RLMI_VConsoleKeymap_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

CMMethodMIStub(
    RLMI_VConsoleKeymap,
    RLMI_VConsoleKeymap,
    _cb,
    RLMI_VConsoleKeymapInitialize())

KUint32 RLMI_VConsoleKeymap_VerifyOKToApplyToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_VConsoleKeymapRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_VConsoleKeymap_ApplyToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_VConsoleKeymapRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_VConsoleKeymap_VerifyOKToApplyToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_VConsoleKeymapRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_VConsoleKeymap_ApplyToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_VConsoleKeymapRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KBoolean* ContinueOnError,
    const KDateTime* MustBeCompletedBy,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_VConsoleKeymap_VerifyOKToApplyIncrementalChangeToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_VConsoleKeymapRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_VConsoleKeymap_ApplyIncrementalChangeToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_VConsoleKeymapRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_VConsoleKeymap_VerifyOKToApplyIncrementalChangeToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_VConsoleKeymapRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_VConsoleKeymap_ApplyIncrementalChangeToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_VConsoleKeymapRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KBoolean* ContinueOnError,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_VConsoleKeymap",
    "RLMI_VConsoleKeymap",
    "instance method")
