#include <konkret/konkret.h>
#include <limits.h>
#include "RLMI_TimeZone.h"

static const CMPIBroker* _cb = NULL;

static void RLMI_TimeZoneInitialize()
{
}

#define WHITESPACE " \t\n\r"

char *strstrip(char *s) {
    /* Drops leading and trailing whitespace. Modifies the string in
     * place. Returns pointer to first non-space character */
    char *e;
    s += strspn(s, WHITESPACE);
    for (e = strchr(s, 0); e > s; e --)
        if (!strchr(WHITESPACE, e[-1]))
            break;
    *e = 0;
    return s;
}

static CMPIStatus RLMI_TimeZoneCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_TimeZoneEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

/* Based on systemd/src/shared/time-util.c:get_timezones */

#define FOREACH_LINE(line, f, on_error)                         \
        for (;;)                                                \
                if (!fgets(line, sizeof(line), f)) {            \
                        if (ferror(f)) {                        \
                                on_error;                       \
                        }                                       \
                        break;                                  \
                } else


static CMPIStatus RLMI_TimeZoneEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    char l[LINE_MAX];
    char *lr;
    char *coco;
    char *coord;
    char *tz;
    char *comment;
    RLMI_TimeZone obj;

    FILE *f = fopen("/usr/share/zoneinfo/zone.tab", "re");
    if (!f) CMReturn(CMPI_RC_ERR_FAILED);

    //To add UTC timezone as systemd:get_timezones does?
    FOREACH_LINE(l, f, break) {

            lr = l + strspn(l, WHITESPACE);
            if ((*lr == 0) || (*lr == '#')) continue;
            lr = l;
            coco = strsep(&lr, "\t");
            if (*coco && *lr) {
                coord = strsep(&lr, "\t");
                if (*coord && *lr) {
                    tz = strsep(&lr, "\t");
                    if (*tz) {
                        coco = strstrip(coco);
                        coord = strstrip(coord);
                        tz = strstrip(tz);
                        comment = lr ? strstrip(lr) : NULL;
                        RLMI_TimeZone_Init(&obj, _cb, KNameSpace(cop));
                        RLMI_TimeZone_Set_SettingID(&obj, tz);
                        RLMI_TimeZone_Set_CountryCode(&obj, coco);
                        RLMI_TimeZone_Set_Coordinates(&obj, coord);
                        if (comment && *comment)
                            RLMI_TimeZone_Set_Comment(&obj, comment);
                        KReturnInstance(cr, obj);
                    }
                }
            }
    }

    fclose(f);
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_TimeZoneGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_TimeZoneCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_TimeZoneModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_TimeZoneDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_TimeZoneExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    RLMI_TimeZone,
    RLMI_TimeZone,
    _cb,
    RLMI_TimeZoneInitialize())

static CMPIStatus RLMI_TimeZoneMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_TimeZoneInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return RLMI_TimeZone_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

CMMethodMIStub(
    RLMI_TimeZone,
    RLMI_TimeZone,
    _cb,
    RLMI_TimeZoneInitialize())

KUint32 RLMI_TimeZone_VerifyOKToApplyToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_TimeZoneRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_TimeZone_ApplyToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_TimeZoneRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_TimeZone_VerifyOKToApplyToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_TimeZoneRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_TimeZone_ApplyToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_TimeZoneRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KBoolean* ContinueOnError,
    const KDateTime* MustBeCompletedBy,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_TimeZone_VerifyOKToApplyIncrementalChangeToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_TimeZoneRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_TimeZone_ApplyIncrementalChangeToMSE(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_TimeZoneRef* self,
    const KRef* MSE,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_TimeZone_VerifyOKToApplyIncrementalChangeToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_TimeZoneRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 RLMI_TimeZone_ApplyIncrementalChangeToCollection(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_TimeZoneRef* self,
    const KRef* Collection,
    const KDateTime* TimeToApply,
    const KBoolean* ContinueOnError,
    const KDateTime* MustBeCompletedBy,
    const KStringA* PropertiesToApply,
    KStringA* CanNotApply,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_TimeZone",
    "RLMI_TimeZone",
    "instance method")
