/*
 * Copyright (C) 2014 Rosalab.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#include <stdlib.h>
#include <string.h>

#include "rosacim-helpers.h"

const char *substr_with_prefix(const char *str, const char *prefix) {
    int plen = strlen(prefix);
    return ((strncmp(str, prefix, plen) == 0) ? str + plen : NULL);
}

char *strstrip(char *s) {
    /* Drops leading and trailing whitespace. Modifies the string in
     * place. Returns pointer to first non-space character */
    char *e;
    s += strspn(s, WHITESPACE);
    for (e = strchr(s, 0); e > s; e --)
        if (!strchr(WHITESPACE, e[-1]))
            break;
    *e = 0;
    return s;
}

char *endswith(const char *s, const char *postfix) {
        size_t sl, pl;

        if (!s || !postfix) return NULL;
        sl = strlen(s);
        pl = strlen(postfix);
        if (pl == 0)
                return (char*) s + sl;
        if (sl < pl)
                return NULL;
        if (memcmp(s + sl - pl, postfix, pl) != 0)
                return NULL;
        return (char*) s + sl - pl;
}
