
#include <konkret/konkret.h>
#include <strings.h>
#include "RLMI_Test.h"
static const CMPIBroker* _cb = NULL;

static void RLMI_TestInitialize()
{
    printf("RLMI_TestInitialize invoked\n");
}

static CMPIStatus RLMI_TestCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_TestEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus RLMI_TestEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    RLMI_Test ti;
    /* TestInstance.Id="1001" */
    RLMI_Test_Init(&ti, _cb, KNameSpace(cop));
    RLMI_Test_Set_Id(&ti, "1001");
    RLMI_Test_Set_Color(&ti, "Red");
    RLMI_Test_Set_Size(&ti, 1);
    KReturnInstance(cr, ti);
    /* TestInstance.Id="1002" */
    RLMI_Test_Init(&ti, _cb, KNameSpace(cop));
    RLMI_Test_Set_Id(&ti, "1002");
    RLMI_Test_Set_Color(&ti, "Green");
    RLMI_Test_Set_Size(&ti, 2);
    KReturnInstance(cr, ti);
    /* TestInstance.Id="1003" */
    RLMI_Test_Init(&ti, _cb, KNameSpace(cop));
    RLMI_Test_Set_Id(&ti, "1003");
    RLMI_Test_Set_Color(&ti, "Blue");
    RLMI_Test_Set_Size(&ti, 3);
    KReturnInstance(cr, ti);
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_TestGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_TestCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_TestModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_TestDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_TestExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    RLMI_Test,
    RLMI_Test,
    _cb,
    RLMI_TestInitialize())
static CMPIStatus RLMI_TestMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_TestInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return RLMI_Test_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

CMMethodMIStub(
    RLMI_Test,
    RLMI_Test,
    _cb,
    RLMI_TestInitialize())
KUint32 RLMI_Test_Add(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_TestRef* self,
    const KUint32* X,
    const KUint32* Y,
    CMPIStatus* status)
{
    printf("RLMI_Test_Add method invoked\n");
    KUint32 result = KUINT32_INIT;
    if (!X->exists || !Y->exists || X->null || Y->null) {
        KSetStatus(status, ERR_INVALID_PARAMETER);
        return result;
    }
    KUint32_Set(&result, X->value + Y->value);
    KSetStatus(status, OK);
    printf("result: %d\n", result.value);
    return result;
}

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_Test",
    "RLMI_Test",
    "instance method")
