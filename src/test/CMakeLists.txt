set(PROVIDERS_MODULE "RLMITestModule")
set(LIBRARY_NAME cmpiRLMI_Test)
set(PROVAGT_SCRIPT ${PROVIDERS_MODULE}-cimprovagt)
set(MOFS 00_RLMI_Test.mof)
set(SRCS "")

source_module_process(${PROVIDERS_MODULE} ${LIBRARY_NAME} "${MOFS}" "${SRCS}" "" "")
