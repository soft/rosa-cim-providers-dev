#!/usr/bin/python

import lmiwbem

conn = lmiwbem.WBEMConnection()
conn.connectLocally()

inames = conn.EnumerateInstanceNames("RLMI_Test1", "root/cimv2")
print inames
ret = conn.InvokeMethod('Add', inames[0],
    X=lmiwbem.Uint32(22),
    Y=lmiwbem.Uint32(33))
print "result: %s" % ret[0]
print "error: %s" % ret[1]
conn.disconnect()