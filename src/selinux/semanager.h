#ifndef SELINUX_MANAGER_H
#define SELINUX_MANAGER_H

#include <semanage/handle.h>

#ifdef  __cplusplus
extern "C"
{
#endif

extern semanage_handle_t *se_handle;

int init_semanager();

int free_semanager();

#ifdef  __cplusplus
}
#endif

#endif /* SELINUX_MANAGER_H */
