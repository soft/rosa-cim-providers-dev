#include <stdlib.h>
#include <pthread.h>

#include "semanager.h"
#include "rosacim.h"

semanage_handle_t *se_handle = NULL;

static pthread_mutex_t _init_mutex = PTHREAD_MUTEX_INITIALIZER;

static unsigned int seman_ref_count = 0;

int init_semanager() {
    int err = -1;

    pthread_mutex_lock(&_init_mutex);

    if (seman_ref_count == 0) {
        se_handle = semanage_handle_create();
        if (semanage_is_managed(se_handle)) {
            err = semanage_connect(se_handle);
            if (err)
                lmi_error("Semanage connect failed (%d).", err);
        } else
            lmi_error("Semanage initialization failed (semanage is not managed).");
        if (se_handle && err) {
            semanage_handle_destroy(se_handle);
            se_handle = NULL;
        }
    }
    seman_ref_count++;

    pthread_mutex_unlock(&_init_mutex);
    return 0;
}

int free_semanager() {
    pthread_mutex_lock(&_init_mutex);
    seman_ref_count--;
    if (seman_ref_count == 0) {
        if (se_handle) {
            semanage_handle_destroy(se_handle);
            se_handle = NULL;
        }
    }
    pthread_mutex_unlock(&_init_mutex);
    return 0;
}
