#include <konkret/konkret.h>
#include "RLMI_SELinuxRole.h"

#include "rosacim.h"
#include "policy.h"

static const CMPIBroker* _cb = NULL;

static void RLMI_SELinuxRoleInitialize()
{
    init_qapolicy();
}

static CMPIStatus RLMI_SELinuxRoleCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    free_qapolicy();
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_SELinuxRoleEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus RLMI_SELinuxRoleEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    RLMI_SELinuxRole obj;
    qpol_iterator_t *iter = NULL;
    const qpol_role_t *role_datum = NULL;
    const char* role_name;
    size_t n_roles = 0;

    int err = -1;
    if (qpolicy) {
        if ((err = qpol_policy_get_role_iter(qpolicy, &iter)) == 0) {
            if ((err = qpol_iterator_get_size(iter, &n_roles)) == 0) {
                for (; !qpol_iterator_end(iter); qpol_iterator_next(iter)) {
                    if ((err = qpol_iterator_get_item(iter, (void **)&role_datum)) != 0)
                        break;
                    if (qpol_role_get_name(qpolicy, role_datum, &role_name) == 0) {
                        RLMI_SELinuxRole_Init(&obj, _cb, KNameSpace(cop));
                        RLMI_SELinuxRole_Set_Name(&obj, role_name);
                        RLMI_SELinuxRole_Set_ElementName(&obj, role_name);
                        KReturnInstance(cr, obj);
                    }
//TODO: qpol_role_get_value, qpol_get_dominate_iter, qpol_get_type_iter
                }
            } else lmi_error("Couldn't get size of qpol_iterator (%d).", err);
            qpol_iterator_destroy(&iter);
        } else lmi_error("Couldn't get role_iter for qpol_policy (%d).", err);
    }
    CMReturn(err ? CMPI_RC_ERR_FAILED : CMPI_RC_OK);
}

static CMPIStatus RLMI_SELinuxRoleGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_SELinuxRoleCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_SELinuxRoleModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_SELinuxRoleDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_SELinuxRoleExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    RLMI_SELinuxRole,
    RLMI_SELinuxRole,
    _cb,
    RLMI_SELinuxRoleInitialize())

static CMPIStatus RLMI_SELinuxRoleMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_SELinuxRoleInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return RLMI_SELinuxRole_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

CMMethodMIStub(
    RLMI_SELinuxRole,
    RLMI_SELinuxRole,
    _cb,
    RLMI_SELinuxRoleInitialize())

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_SELinuxRole",
    "RLMI_SELinuxRole",
    "instance method")
