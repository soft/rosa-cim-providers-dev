#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include <apol/policy.h>
#include <qpol/util.h>

#include "policy.h"
#include "rosacim.h"

static pthread_mutex_t _init_mutex = PTHREAD_MUTEX_INITIALIZER;

apol_policy_t *policydb = NULL;
qpol_policy_t *qpolicy = NULL;
static apol_policy_path_t *pol_path = NULL;

static unsigned int policy_ref_count = 0;

int init_qapolicy() {
    int err = -1;
    char *policy_file = NULL;
    int policy_load_options = QPOL_POLICY_OPTION_NO_RULES;

    pthread_mutex_lock(&_init_mutex);

    if (policy_ref_count == 0) {
        err = qpol_default_policy_find(&policy_file);
        if (!err) {
            pol_path = apol_policy_path_create(APOL_POLICY_PATH_TYPE_MONOLITHIC, policy_file, NULL);
            if (pol_path) {
                if ((policydb = apol_policy_create_from_policy_path(pol_path, policy_load_options, NULL, NULL)) != NULL) {
                    qpolicy = apol_policy_get_qpol(policydb);
                    if (!qpolicy) {
                        lmi_error("Couldn't get qpolicy.");
                        apol_policy_destroy(&policydb);
                        policydb = NULL;
                    }
                } else lmi_error("Couldn't create apol_policy from policy_path.");
            } else lmi_error("Couldn't create apol_policy_path.");
        } else lmi_error("Couldn't find qpol_default_policy (%d).", err);
        free(policy_file);
    }
    policy_ref_count++;

    pthread_mutex_unlock(&_init_mutex);
    return 0;
}

int free_qapolicy() {
    pthread_mutex_lock(&_init_mutex);
    policy_ref_count--;
    if (policy_ref_count == 0) {
        if (policydb) {
            apol_policy_destroy(&policydb);
            policydb = NULL;
        }
        if (pol_path) {
            apol_policy_path_destroy(&pol_path);
            pol_path = NULL;
        }
    }
    pthread_mutex_unlock(&_init_mutex);
    return 0;
}
