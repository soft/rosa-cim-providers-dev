#ifndef SELINUX_POLICY_H
#define SELINUX_POLICY_H

#include <apol/policy.h>
#include <qpol/util.h>

#ifdef  __cplusplus
extern "C"
{
#endif

extern apol_policy_t *policydb;
extern qpol_policy_t *qpolicy;

int init_qapolicy();

int free_qapolicy();

#ifdef  __cplusplus
}
#endif

#endif /* SELINUX_POLICY_H */
