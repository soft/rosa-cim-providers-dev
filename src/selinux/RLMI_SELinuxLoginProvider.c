#include <konkret/konkret.h>
#include "RLMI_SELinuxLogin.h"

#include <semanage/seuser_record.h>
#include <semanage/seusers_policy.h>

#include "rosacim.h"
#include "semanager.h"

static const CMPIBroker* _cb = NULL;

static void RLMI_SELinuxLoginInitialize()
{
    init_semanager();
}

static CMPIStatus RLMI_SELinuxLoginCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    free_semanager();
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_SELinuxLoginEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus RLMI_SELinuxLoginEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    RLMI_SELinuxLogin obj;
    const char* user_name;
    const char* user_sename;
    semanage_seuser_t** seuser_list = NULL;
    unsigned int count;

    int err = -1;

    if (!se_handle) {
        CMReturn(CMPI_RC_ERR_FAILED);
    }

    if (semanage_seuser_list(se_handle, &seuser_list, &count) >= 0) {
        err = 0;
        for (unsigned int i = 0; i < count; i++) {
            user_name = semanage_seuser_get_name(seuser_list[i]);
            user_sename = semanage_seuser_get_sename(seuser_list[i]);

            RLMI_SELinuxLogin_Init(&obj, _cb, KNameSpace(cop));
            RLMI_SELinuxLogin_Set_Name(&obj, user_name);
            RLMI_SELinuxLogin_Set_ElementName(&obj, user_name);
            RLMI_SELinuxLogin_Set_UserName(&obj, user_sename);
            KReturnInstance(cr, obj);

            semanage_seuser_free(seuser_list[i]);
        }
        free(seuser_list);
    } else
        lmi_error("Couldn't get semenage_seuser_list.");

    CMReturn(err ? CMPI_RC_ERR_FAILED : CMPI_RC_OK);
}

static CMPIStatus RLMI_SELinuxLoginGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_SELinuxLoginCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_SELinuxLoginModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_SELinuxLoginDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_SELinuxLoginExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    RLMI_SELinuxLogin,
    RLMI_SELinuxLogin,
    _cb,
    RLMI_SELinuxLoginInitialize())

static CMPIStatus RLMI_SELinuxLoginMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_SELinuxLoginInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return RLMI_SELinuxLogin_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

CMMethodMIStub(
    RLMI_SELinuxLogin,
    RLMI_SELinuxLogin,
    _cb,
    RLMI_SELinuxLoginInitialize())

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_SELinuxLogin",
    "RLMI_SELinuxLogin",
    "instance method")
