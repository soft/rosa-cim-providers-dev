#include <konkret/konkret.h>
#include "RLMI_SELinuxUser.h"

#include <semanage/user_record.h>
#include <semanage/users_policy.h>

#include "rosacim.h"
#include "semanager.h"

static const CMPIBroker* _cb = NULL;

static void RLMI_SELinuxUserInitialize()
{
    init_semanager();
}

static CMPIStatus RLMI_SELinuxUserCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    free_semanager();
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_SELinuxUserEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus RLMI_SELinuxUserEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    RLMI_SELinuxUser obj;
    const char* user_name;
    semanage_user_t** user_list = NULL;
    unsigned int count;

    int err = -1;

    if (!se_handle) {
        CMReturn(CMPI_RC_ERR_FAILED);
    }

    if (semanage_user_list(se_handle, &user_list, &count) >= 0) {
        err = 0;
        for (unsigned int i = 0; i < count; i++) {
            user_name = semanage_user_get_name(user_list[i]);

            RLMI_SELinuxUser_Init(&obj, _cb, KNameSpace(cop));
            RLMI_SELinuxUser_Set_Name(&obj, user_name);
            RLMI_SELinuxUser_Set_ElementName(&obj, user_name);
            KReturnInstance(cr, obj);

            semanage_user_free(user_list[i]);
        }
        free(user_list);
    } else
        lmi_error("Couldn't get semenage_user_list.");

    CMReturn(err ? CMPI_RC_ERR_FAILED : CMPI_RC_OK);
}

static CMPIStatus RLMI_SELinuxUserGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_SELinuxUserCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_SELinuxUserModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_SELinuxUserDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_SELinuxUserExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    RLMI_SELinuxUser,
    RLMI_SELinuxUser,
    _cb,
    RLMI_SELinuxUserInitialize())

static CMPIStatus RLMI_SELinuxUserMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_SELinuxUserInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return RLMI_SELinuxUser_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

CMMethodMIStub(
    RLMI_SELinuxUser,
    RLMI_SELinuxUser,
    _cb,
    RLMI_SELinuxUserInitialize())

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_SELinuxUser",
    "RLMI_SELinuxUser",
    "instance method")
