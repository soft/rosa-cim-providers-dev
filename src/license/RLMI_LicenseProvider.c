#include <konkret/konkret.h>
#define _GNU_SOURCE
#include <stdio.h>

#include "RLMI_License.h"


static const CMPIBroker* _cb = NULL;

static const char* defaultLangs = "en,ru";
static const char* license_path = "/usr/share/kcm_license/license_files/license_%s.html";
#define MAX_SIZE 1000000

#define LICENSE_ID "ROSA_OS_LICENSE"

static char* readOSLicense(const char *lang) {
    char *path;
    char *contents = NULL;
    size_t size;
    size_t t;
    if (!((*lang >= 'a' && *lang <= 'z') ||
          (*lang >= 'A' && *lang <= 'Z')))
        return NULL;
    if (asprintf(&path, license_path, lang) < 0)
        return NULL;
    FILE *fh = fopen(path, "rt");
    if (fh) {
        fseek(fh, 0, SEEK_END);
        size = ftell(fh);
        if (size < MAX_SIZE) {
            rewind(fh);
            contents = malloc(size + 1);
            if (contents) {
                if ((t = fread(contents, 1, size, fh)) == size) {
                    contents[size] = 0;
                } else {
                    free(contents);
                    contents = NULL;
                }
            }
        }
        fclose(fh);
    }
    free(path);
    return contents;
}

static void RLMI_LicenseInitialize()
{
}

static CMPIStatus RLMI_LicenseCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_LicenseEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus RLMI_LicenseEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    CMPIStatus st = {0, NULL};
    CMPIData alData;
    const char* langs = NULL;
    char *str;
    char* p;
    char* lp;
    char *license_text;
    RLMI_License obj;

    alData = CMGetContextEntry(cc, CMPIAcceptLanguage, &st);
    if (st.rc == CMPI_RC_OK) {
        langs = CMGetCharsPtr(alData.value.string, NULL);
    }
    if (!langs || !*langs) langs = defaultLangs;
    if ((str = strdup(langs)) != NULL) {
        for (p = str, lp = str; *p; p++) {
            if (*p == ',') {
                *p = 0;
                license_text = readOSLicense(lp);
                if (license_text) break;
                lp = p + 1;
            } else if (*p == ';') *p = 0;
        }
        if (!license_text && *lp) license_text = readOSLicense(lp);
        free(str);
        if (license_text) {
            RLMI_License_Init(&obj, _cb, KNameSpace(cop));
            RLMI_License_Set_Id(&obj, LICENSE_ID);
            RLMI_License_Set_Text(&obj, license_text);
            KReturnInstance(cr, obj);
            free(license_text);
        }
    }

    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_LicenseGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_LicenseCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_LicenseModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_LicenseDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_LicenseExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    RLMI_License,
    RLMI_License,
    _cb,
    RLMI_LicenseInitialize())

static CMPIStatus RLMI_LicenseMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_LicenseInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return RLMI_License_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

CMMethodMIStub(
    RLMI_License,
    RLMI_License,
    _cb,
    RLMI_LicenseInitialize())

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_License",
    "RLMI_License",
    "instance method")
