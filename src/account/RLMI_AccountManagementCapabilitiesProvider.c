/*
 * Copyright (C) 2012-2014 Red Hat, Inc.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Authors: Roman Rakus <rrakus@redhat.com>
 */

#include <konkret/konkret.h>
#include "RLMI_AccountManagementCapabilities.h"

#include <stdbool.h>

#include "macros.h"
#include "account_globals.h"

#define NAME LAMCNAME

static const CMPIBroker* _cb = NULL;

static void RLMI_AccountManagementCapabilitiesInitialize(const CMPIContext *ctx)
{
    lmi_init(provider_name, _cb, ctx, provider_config_defaults);
}

static CMPIStatus RLMI_AccountManagementCapabilitiesCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_AccountManagementCapabilitiesEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus RLMI_AccountManagementCapabilitiesEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    RLMI_AccountManagementCapabilities lamc;
    int size = 0; /* How many encrypt algorithms we have */
    int i = 0;

    RLMI_AccountManagementCapabilities_Init(&lamc, _cb, KNameSpace(cop));
    RLMI_AccountManagementCapabilities_Set_ElementNameEditSupported(
      &lamc, false);
    RLMI_AccountManagementCapabilities_Set_InstanceID(&lamc, LMI_ORGID":"LAMCNAME);
    RLMI_AccountManagementCapabilities_Set_ElementName(&lamc, NAME);

    RLMI_AccountManagementCapabilities_Init_OperationsSupported(&lamc, 3);
    RLMI_AccountManagementCapabilities_Set_OperationsSupported(&lamc, 0,
      RLMI_AccountManagementCapabilities_OperationsSupported_Create);
    RLMI_AccountManagementCapabilities_Set_OperationsSupported(&lamc, 1,
      RLMI_AccountManagementCapabilities_OperationsSupported_Modify);
    RLMI_AccountManagementCapabilities_Set_OperationsSupported(&lamc, 2,
      RLMI_AccountManagementCapabilities_OperationsSupported_Delete);

    RLMI_AccountManagementCapabilities_Init_SupportedUserPasswordEncryptionAlgorithms(&lamc, 1);
    RLMI_AccountManagementCapabilities_Set_SupportedUserPasswordEncryptionAlgorithms(&lamc, 0, RLMI_AccountManagementCapabilities_SupportedUserPasswordEncryptionAlgorithms_Other);

    /* Count how many ecrypt algorithms we have */
    while(crypt_algs[size]) size++;

    /* Create a list of other supported password encrypt algorithms */
    if (size > 0)
      {
        RLMI_AccountManagementCapabilities_Init_OtherSupportedUserPasswordEncryptionAlgorithms(&lamc, size);
        for (i = 0; i < size; i++)
          {
            RLMI_AccountManagementCapabilities_Set_OtherSupportedUserPasswordEncryptionAlgorithms(&lamc, i, crypt_algs[i]);
          }
      }

    KReturnInstance(cr, lamc);
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_AccountManagementCapabilitiesGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_AccountManagementCapabilitiesCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_AccountManagementCapabilitiesModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_AccountManagementCapabilitiesDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_AccountManagementCapabilitiesExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    RLMI_AccountManagementCapabilities,
    RLMI_AccountManagementCapabilities,
    _cb,
    RLMI_AccountManagementCapabilitiesInitialize(ctx))

static CMPIStatus RLMI_AccountManagementCapabilitiesMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_AccountManagementCapabilitiesInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return RLMI_AccountManagementCapabilities_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

KUint16 RLMI_AccountManagementCapabilities_CreateGoalSettings(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const RLMI_AccountManagementCapabilitiesRef* self,
    const KInstanceA* TemplateGoalSettings,
    KInstanceA* SupportedGoalSettings,
    CMPIStatus* status)
{
    KUint16 result = KUINT16_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

CMMethodMIStub(
    RLMI_AccountManagementCapabilities,
    RLMI_AccountManagementCapabilities,
    _cb,
    RLMI_AccountManagementCapabilitiesInitialize(ctx))

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_AccountManagementCapabilities",
    "RLMI_AccountManagementCapabilities",
    "instance method")
