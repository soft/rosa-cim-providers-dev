/*
 * Copyright (C) 2012-2014 Red Hat, Inc.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Authors: Roman Rakus <rrakus@redhat.com>
 */

#include <konkret/konkret.h>
#include "RLMI_HostedAccountManagementService.h"
#include "CIM_ComputerSystem.h"
#include "RLMI_AccountManagementService.h"

#include "macros.h"
#include "account_globals.h"

static const CMPIBroker* _cb;

static void RLMI_HostedAccountManagementServiceInitialize(const CMPIContext *ctx)
{
    lmi_init(provider_name, _cb, ctx, provider_config_defaults);
}

static CMPIStatus RLMI_HostedAccountManagementServiceCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_HostedAccountManagementServiceEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus RLMI_HostedAccountManagementServiceEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    RLMI_AccountManagementServiceRef lamsref;
    RLMI_HostedAccountManagementService lhs;

    const char *nameSpace = KNameSpace(cop);
    const char *hostname = lmi_get_system_name_safe(cc);

    RLMI_AccountManagementServiceRef_Init(&lamsref, _cb, nameSpace);
    RLMI_AccountManagementServiceRef_Set_Name(&lamsref, LAMSNAME);
    RLMI_AccountManagementServiceRef_Set_SystemCreationClassName(&lamsref,
      lmi_get_system_creation_class_name());
    RLMI_AccountManagementServiceRef_Set_SystemName(&lamsref, hostname);
    RLMI_AccountManagementServiceRef_Set_CreationClassName(&lamsref,
      RLMI_AccountManagementService_ClassName);

    RLMI_HostedAccountManagementService_Init(&lhs, _cb, nameSpace);
    RLMI_HostedAccountManagementService_SetObjectPath_Antecedent(&lhs,
            lmi_get_computer_system_safe(cc));
    RLMI_HostedAccountManagementService_Set_Dependent(&lhs, &lamsref);

    KReturnInstance(cr, lhs);
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_HostedAccountManagementServiceGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_HostedAccountManagementServiceCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_HostedAccountManagementServiceModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char**properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_HostedAccountManagementServiceDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_HostedAccountManagementServiceExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_HostedAccountManagementServiceAssociationCleanup(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_HostedAccountManagementServiceAssociators(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* resultClass,
    const char* role,
    const char* resultRole,
    const char** properties)
{
    return KDefaultAssociators(
        _cb,
        mi,
        cc,
        cr,
        cop,
        RLMI_HostedAccountManagementService_ClassName,
        assocClass,
        resultClass,
        role,
        resultRole,
        properties);
}

static CMPIStatus RLMI_HostedAccountManagementServiceAssociatorNames(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* resultClass,
    const char* role,
    const char* resultRole)
{
    return KDefaultAssociatorNames(
        _cb,
        mi,
        cc,
        cr,
        cop,
        RLMI_HostedAccountManagementService_ClassName,
        assocClass,
        resultClass,
        role,
        resultRole);
}

static CMPIStatus RLMI_HostedAccountManagementServiceReferences(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* role,
    const char** properties)
{
    return KDefaultReferences(
        _cb,
        mi,
        cc,
        cr,
        cop,
        RLMI_HostedAccountManagementService_ClassName,
        assocClass,
        role,
        properties);
}

static CMPIStatus RLMI_HostedAccountManagementServiceReferenceNames(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* role)
{
    return KDefaultReferenceNames(
        _cb,
        mi,
        cc,
        cr,
        cop,
        RLMI_HostedAccountManagementService_ClassName,
        assocClass,
        role);
}

CMInstanceMIStub(
    RLMI_HostedAccountManagementService,
    RLMI_HostedAccountManagementService,
    _cb,
    RLMI_HostedAccountManagementServiceInitialize(ctx))

CMAssociationMIStub(
    RLMI_HostedAccountManagementService,
    RLMI_HostedAccountManagementService,
    _cb,
    RLMI_HostedAccountManagementServiceInitialize(ctx))

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_HostedAccountManagementService",
    "RLMI_HostedAccountManagementService",
    "instance association")
