/*
 * Copyright (C) 2012-2014 Red Hat, Inc.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Authors: Roman Rakus <rrakus@redhat.com>
 */

#include <konkret/konkret.h>
#include "RLMI_AccountManagementServiceCapabilities.h"
#include "RLMI_AccountManagementService.h"
#include "RLMI_AccountManagementCapabilities.h"

#include "macros.h"
#include "account_globals.h"

static const CMPIBroker* _cb;

static void RLMI_AccountManagementServiceCapabilitiesInitialize(const CMPIContext *ctx)
{
    lmi_init(provider_name, _cb, ctx, provider_config_defaults);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    RLMI_AccountManagementServiceRef lamsref;
    RLMI_AccountManagementCapabilitiesRef lamcref;
    RLMI_AccountManagementServiceCapabilities lamsc;

    const char *nameSpace = KNameSpace(cop);
    const char *hostname = lmi_get_system_name_safe(cc);

    RLMI_AccountManagementServiceRef_Init(&lamsref, _cb, nameSpace);
    RLMI_AccountManagementServiceRef_Set_Name(&lamsref, LAMSNAME);
    RLMI_AccountManagementServiceRef_Set_SystemCreationClassName(&lamsref,
      lmi_get_system_creation_class_name());
    RLMI_AccountManagementServiceRef_Set_SystemName(&lamsref, hostname);
    RLMI_AccountManagementServiceRef_Set_CreationClassName(&lamsref,
      RLMI_AccountManagementService_ClassName);

    RLMI_AccountManagementCapabilitiesRef_Init(&lamcref, _cb, nameSpace);
    RLMI_AccountManagementCapabilitiesRef_Set_InstanceID(&lamcref,
      LMI_ORGID":"LAMCNAME);

    RLMI_AccountManagementServiceCapabilities_Init(&lamsc, _cb, nameSpace);
    RLMI_AccountManagementServiceCapabilities_Set_ManagedElement(&lamsc,
      &lamsref);
    RLMI_AccountManagementServiceCapabilities_Set_Capabilities(&lamsc,
      &lamcref);

    KReturnInstance(cr, lamsc);

    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char**properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesAssociationCleanup(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesAssociators(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* resultClass,
    const char* role,
    const char* resultRole,
    const char** properties)
{
    return KDefaultAssociators(
        _cb,
        mi,
        cc,
        cr,
        cop,
        RLMI_AccountManagementServiceCapabilities_ClassName,
        assocClass,
        resultClass,
        role,
        resultRole,
        properties);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesAssociatorNames(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* resultClass,
    const char* role,
    const char* resultRole)
{
    return KDefaultAssociatorNames(
        _cb,
        mi,
        cc,
        cr,
        cop,
        RLMI_AccountManagementServiceCapabilities_ClassName,
        assocClass,
        resultClass,
        role,
        resultRole);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesReferences(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* role,
    const char** properties)
{
    return KDefaultReferences(
        _cb,
        mi,
        cc,
        cr,
        cop,
        RLMI_AccountManagementServiceCapabilities_ClassName,
        assocClass,
        role,
        properties);
}

static CMPIStatus RLMI_AccountManagementServiceCapabilitiesReferenceNames(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* role)
{
    return KDefaultReferenceNames(
        _cb,
        mi,
        cc,
        cr,
        cop,
        RLMI_AccountManagementServiceCapabilities_ClassName,
        assocClass,
        role);
}

CMInstanceMIStub(
    RLMI_AccountManagementServiceCapabilities,
    RLMI_AccountManagementServiceCapabilities,
    _cb,
    RLMI_AccountManagementServiceCapabilitiesInitialize(ctx))

CMAssociationMIStub(
    RLMI_AccountManagementServiceCapabilities,
    RLMI_AccountManagementServiceCapabilities,
    _cb,
    RLMI_AccountManagementServiceCapabilitiesInitialize(ctx))

KONKRET_REGISTRATION(
    "root/cimv2",
    "RLMI_AccountManagementServiceCapabilities",
    "RLMI_AccountManagementServiceCapabilities",
    "instance association")
