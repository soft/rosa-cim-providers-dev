#!/usr/bin/python
#
# Copyright (C) 2014 Rosalab.  All rights reserved.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
#

import sys
import os
import re

reg_parse = re.compile(r"\[([^\]]+)\]\s+"
"provider: ([^\s]+)\s+"
"location: (\w+)\s+"
"type: ([^\n]+)\s+"
"namespace: ([^\n]+)\s+"
"(group: ([^\n]+)|)") # the group is optional

Types = {
    'instance': '2',
    'association': '3',
    'indication': '4',
    'method': '5',
    'consumer': '6',
    'instanceQuery': '7'
}

def get_types(types):
    l = []
    for key, value in Types.items():
        if key in types:
            l.append(value)
    return ",".join(l)

def define_module(location, group, version):
    return """instance of PG_ProviderModule
{
    Name = "%(group)s";
    Location = "%(location)s";
    Vendor = "RosaLab";
    Version = "%(version)s";
    InterfaceType = "CMPI";
    InterfaceVersion = "2.0.0";
    ModuleGroupName = "%(group)s";
};
""" % { 'location': location, 'group': group, 'version': version }

def define_capability(provider, cls, types, module):
    return """instance of PG_Provider
{
    Name = "%(provider)s";
    ProviderModuleName = "%(module)s";
};

instance of PG_ProviderCapabilities
{
   ProviderModuleName = "%(module)s";
   ProviderName = "%(provider)s";
   CapabilityID = "%(class)s";
   ClassName = "%(class)s";
   Namespaces = { "root/cimv2" };
   ProviderType = { %(types)s };
   SupportedProperties = NULL;
   SupportedMethods = NULL;
};
""" % { 'provider': provider, 'class': cls, 'types': get_types(types), 'module': module }

def register_pegasus(reg, version, cimmof, cm_args):
    modules_defined = {}
    reg_data = regfile.read()
    tmpfile = NamedTemporaryFile()
    for record in reg_parse.findall(reg_data):
        cls, provider, location, types, namespace, _unused, group = record

        if not group:
            group = location

        if group not in modules_defined:
            tmpfile.write(define_module(location, group, version))
            modules_defined[group] = True

        tmpfile.write(define_capability(location, provider, cls, types, group))
    tmpfile.flush()
    interop_ns = find_pegasus_interop()
    cm_args += ["-uc", "-n", interop_ns, tmpfile.name]
    log_command(cimmof, cm_args)
    tmpfile.close() # deletes the file

def main():
    if (len(sys.argv) != 5):
        sys.exit(1)
    modules_defined = {}
    moffile = open(sys.argv[1], 'w')
    library = sys.argv[2]
    module = sys.argv[3]
    version = sys.argv[4]
    reg_data = sys.stdin.read()
    moffile.write(define_module(library, module, version))
    for record in reg_parse.findall(reg_data):
        cls, provider, location, types, namespace, _unused, group = record
        moffile.write(define_capability(provider, cls, types, module))

    moffile.close()
    sys.exit(0)

if __name__ == "__main__":
    main()
